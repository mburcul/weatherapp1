//
//  WeatherCollectionView.swift
//  WeatherApp
//
//  Created by Marko Burčul on 02/07/2018.
//  Copyright © 2018 Marko Burčul. All rights reserved.
//

import UIKit
import PureLayout
import RxCocoa
import RxSwift

class WeatherTableViewController: UIViewController {
    
    private let viewModel: CityWeatherViewModel = CityWeatherViewModel()
    
    private var searchTextField: UITextField = UITextField()
    private var tableView: UITableView = UITableView()
    
    private let disposeBag: DisposeBag = DisposeBag()
    
    private let cellReuseIdentifier = "weatherCell"
    var searchStringPublisher: PublishSubject<String> = PublishSubject()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildViews()
        setupViews()
        setupTableViewModelBinding()
        viewModel.bindToSearch(publisher: self.searchStringPublisher)
        searchStringPublisher.onNext("") //ok ali postoji sigurno neki bolji način, u takvim slučajevima to je HACK
    }
    
    private func setupViews() {
        navigationController?.navigationBar.isHidden = true
        searchTextField.delegate = self
        searchTextField.layer.cornerRadius = 5
        searchTextField.textAlignment = .center
        searchTextField.layer.backgroundColor = UIColor.white.cgColor
        view.backgroundColor = UIColor(red: 98/255, green: 167/255, blue: 1.0, alpha: 1.0)
        tableView.tableFooterView = UIView()
        tableView.rx.setDelegate(self).disposed(by: disposeBag)
        tableView.separatorColor = UIColor.black
        tableView.register(WeatherTableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
    }
    
    private func setupTableViewModelBinding() {
        let selectionIndex = tableView.rx.itemSelected.asObservable()
        selectionIndex
            .withLatestFrom(viewModel.cities) {($0, $1)}
            .subscribe(onNext: { indexPath, cities in
                //koristi weak i unowned self da spriječiš retain cycle
                self.tableView.deselectRow(at: indexPath, animated: true)
                let weatherDetailsViewController = WeatherDetailsViewController(city: cities[indexPath.row])
                self.navigationController?.pushViewController(weatherDetailsViewController, animated: true)
            }).disposed(by: disposeBag)
        viewModel
            .cities
            .bind(to: tableView.rx.items(cellIdentifier: cellReuseIdentifier, cellType: WeatherTableViewCell.self)){  row, element, cell in
                cell.setup(withCity: element)
                // cell+design
                cell.layer.backgroundColor = UIColor.white.cgColor
                cell.layer.borderColor = UIColor(red: 98/255, green: 167/255, blue: 1.0, alpha: 1.0).cgColor
                cell.layer.borderWidth = 1.0
            }.disposed(by: disposeBag)
        }
    
    private func buildViews() {
        navigationController?.isNavigationBarHidden = true
        view.addSubview(searchTextField)
        searchTextField.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 70, left: 15, bottom: 0, right: 15), excludingEdge: .bottom)
        view.addSubview(tableView)
        tableView.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), excludingEdge: .top)
        tableView.autoPinEdge(.top, to: .bottom, of: searchTextField, withOffset: 30)
        searchTextField.autoSetDimension(.height, toSize: 50)
    }
}

extension WeatherTableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
    }
}

//TODO: rx
extension WeatherTableViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = textField.text {
            if text.isEmpty {
                searchStringPublisher.onNext("")
            } else {
                searchStringPublisher.onNext(text)
            }
        }
        return true
    }
}
