//
//  WeatherTableViewCell.swift
//  WeatherApp
//
//  Created by Marko Burčul on 02/07/2018.
//  Copyright © 2018 Marko Burčul. All rights reserved.
//

import UIKit
import PureLayout


class WeatherTableViewCell: UITableViewCell {

    var cityNameLabel: UILabel = UILabel()
    var weatherIcon: UIImageView = UIImageView()
    var minTempLabel: UILabel = UILabel()
    var maxTempLabel: UILabel = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViews() {
        contentView.addSubview(cityNameLabel)
        contentView.addSubview(weatherIcon)
        contentView.addSubview(minTempLabel)
        contentView.addSubview(maxTempLabel)
        
        cityNameLabel.textAlignment = .center
        cityNameLabel.autoSetDimension(.height, toSize: 50)
        cityNameLabel.autoPinEdgesToSuperviewEdges(with: UIEdgeInsetsMake(15, 30, 0, 30), excludingEdge: .bottom)
        
        cityNameLabel.autoPinEdge(.bottom, to: .top, of: weatherIcon, withOffset: 20)
        weatherIcon.autoPinEdge(.leading, to: .leading, of: contentView, withOffset: 10)
        weatherIcon.autoSetDimension(.height, toSize: 100)
        weatherIcon.autoSetDimension(.width, toSize: 100)
        
        minTempLabel.autoPinEdge(.top, to: .bottom, of: cityNameLabel, withOffset: 30)
        maxTempLabel.autoPinEdge(.top, to: .bottom, of: minTempLabel, withOffset: 10)
        
        minTempLabel.autoPinEdge(.leading, to: .trailing, of: weatherIcon, withOffset: 40)
        maxTempLabel.autoPinEdge(.leading, to: .trailing, of: weatherIcon, withOffset: 40)
        contentView.autoPinEdge(.trailing, to: .trailing, of: minTempLabel, withOffset: 20)
        contentView.autoPinEdge(.trailing, to: .trailing, of: maxTempLabel, withOffset: 20)
        contentView.autoPinEdgesToSuperviewEdges()  
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cityNameLabel.text = ""
        weatherIcon.image = nil
        minTempLabel.text = ""
        maxTempLabel.text = ""
    }
    
    func setup(withCity city: City) {
        cityNameLabel.text = city.name
//        minTempLabel.text = "Temperature min: " + String(city.temperatureMin)
        minTempLabel.text = "Temperature min: \(String(city.temperatureMin))"
        maxTempLabel.text = "Temperature max: " + String(city.temperatureMax)
        weatherIcon.image = UIImage(named: city.iconcode!)
    }
    
}
