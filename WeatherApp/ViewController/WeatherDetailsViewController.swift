//
//  WeatherDetailsViewController.swift
//  WeatherApp
//
//  Created by Marko Burčul on 02/07/2018.
//  Copyright © 2018 Marko Burčul. All rights reserved.
//

import UIKit
import PureLayout

class WeatherDetailsViewController: UIViewController {

    private var cityNameLabel: UILabel = UILabel()
    private var weatherLogo: UIImageView = UIImageView()
    private var weatherDescriptionLabel: UILabel = UILabel()
    private var tempMinLabel: UILabel = UILabel()
    private var tempMaxLabel: UILabel = UILabel()
    private var humidityLabel: UILabel = UILabel()
    private var pressureLabel: UILabel = UILabel()
    
    var cityData:City!
    
    convenience init(city: City){
        self.init()
        self.cityData = city
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
        buildViews()
    }
    
    func buildViews() {
        navigationController?.isNavigationBarHidden = false
        view.addSubview(cityNameLabel)
        view.addSubview(weatherLogo)
        view.addSubview(weatherDescriptionLabel)
        view.addSubview(tempMinLabel)
        view.addSubview(tempMaxLabel)
        view.addSubview(humidityLabel)
        view.addSubview(pressureLabel)
        
        cityNameLabel.autoPinEdgesToSuperviewEdges(with: UIEdgeInsetsMake(40, 50, 0, 50), excludingEdge: .bottom)
        cityNameLabel.textAlignment = .center
        cityNameLabel.autoSetDimension(.height, toSize: 50)
        
        cityNameLabel.autoPinEdge(.bottom, to: .top, of: weatherLogo, withOffset: 30)
        weatherLogo.autoPinEdge(.leading, to: .leading, of: view, withOffset: 50)
        view.autoPinEdge(.trailing, to: .trailing, of: weatherLogo, withOffset: 50)
        weatherLogo.autoSetDimension(.height, toSize: 130)
        weatherLogo.autoSetDimension(.width, toSize: 130)

        weatherDescriptionLabel.textAlignment = .center
        weatherLogo.autoPinEdge(.bottom, to: .top, of: weatherDescriptionLabel, withOffset: 20)
        weatherDescriptionLabel.autoPinEdge(.leading, to: .leading, of: view, withOffset: 90)
        view.autoPinEdge(.trailing, to: .trailing, of: weatherDescriptionLabel, withOffset: 90)

        weatherDescriptionLabel.autoPinEdge(.bottom, to: .top, of: tempMinLabel, withOffset: 60)
        tempMinLabel.autoPinEdge(.leading, to: .leading, of: view, withOffset: 35)
        view.autoPinEdge(.trailing, to: .trailing, of: tempMinLabel, withOffset: 90)

        tempMinLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        tempMinLabel.autoPinEdge(.bottom, to: .top, of: tempMaxLabel, withOffset: 20)
        tempMaxLabel.autoPinEdge(.leading, to: .leading, of: view, withOffset: 35)
        view.autoPinEdge(.trailing, to: .trailing, of: tempMaxLabel, withOffset: 90)

        tempMaxLabel.autoPinEdge(.bottom, to: .top, of: humidityLabel, withOffset: 20)
        humidityLabel.autoPinEdge(.leading, to: .leading, of: view, withOffset: 35)
        view.autoPinEdge(.trailing, to: .trailing, of: humidityLabel, withOffset: 90)

        humidityLabel.autoPinEdge(.bottom, to: .top, of: pressureLabel, withOffset: 20)
        pressureLabel.autoPinEdge(.leading, to: .leading, of: view, withOffset: 35)
        view.autoPinEdge(.trailing, to: .trailing, of: pressureLabel, withOffset: 90)
        
    }
    
    func setupData(){
        self.navigationController?.isNavigationBarHidden = false
        self.cityNameLabel.text = cityData.name
        self.weatherLogo.image = UIImage(named: cityData.iconcode!)
        self.weatherDescriptionLabel.text = cityData.weatherState
        self.tempMinLabel.text = "Temperature min: " + String(cityData.temperatureMin) + " C"
        self.tempMaxLabel.text = "Temperature max: " + String(cityData.temperatureMax) + " C"
        self.humidityLabel.text = "Humidity level: " + String(cityData.humidity) + " %"
        self.pressureLabel.text = "Pressure: " + String(cityData.pressure) + " Pa"
    }

}
