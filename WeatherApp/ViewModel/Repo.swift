//
//  Repo.swift
//  WeatherApp
//
//  Created by Marko Burčul on 05/07/2018.
//  Copyright © 2018 Marko Burčul. All rights reserved.
//

import Foundation
import RxSwift

class Repo {
    
    private var databaseConnection: DBWrapper = DBWrapper()
    
    private var apiConnection: WeatherAPI
    
    var cityFilter: Observable<[City]>!
    
    private var disposeBag: DisposeBag
    
    let defaultCitiesID: String = "3186886,3190261,3193935,3191648"
    
    let defaultCitiesNames: [String] = ["Zagreb", "Osijek", "Rijeka", "Split"]
    
    var searchObserver: PublishSubject<String> = PublishSubject()
    
    init(typeOfUnit: UnitType) {
        apiConnection = WeatherAPI(unitType: typeOfUnit, fetchType: FetchType.Group)
        disposeBag = DisposeBag()
//        cityFilter = Observable.create { observer in
//            return Disposables.create()
//        }
        let dbObserver = databaseConnection.reactiveFetchFromDataBase().asObservable()
        cityFilter = dbObserver
            .withLatestFrom(searchObserver){($0, $1)}
            // bolje nego .map() {....
            .map { cities, searchString in
                let filteredCities =  cities.filter{ city -> Bool in
                    //guard za optional city.name
                    if searchString == "" && self.defaultCitiesNames.contains(city.name!) {
                        return true
                    }
                    return city.name == searchString
                }
            return filteredCities
        }
        
        searchObserver
            .asObserver()
            .subscribe(onNext: { cityName in
            if cityName.count != 0 {
                self.fetchCityData(cityData: cityName, typeOfFetch: FetchType.Single)
            } else {
                self.fetchCityData(cityData: self.defaultCitiesID, typeOfFetch: FetchType.Group)
            }
        }).disposed(by: disposeBag)
    }
    
    func fetchCityData(cityData: String, typeOfFetch: FetchType) {
        apiConnection.typeOfFetch = typeOfFetch
        apiConnection.fetchWeather(cityParameter: cityData)
            .debug()
            .bind(onNext: self.databaseConnection.convertAndStoreJson)
            .disposed(by: disposeBag)
    }
}
