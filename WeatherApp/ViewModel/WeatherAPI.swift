//
//  WeatherAPI.swift
//  WeatherApp
//
//  Created by Marko Burčul on 04/07/2018.
//  Copyright © 2018 Marko Burčul. All rights reserved.
//

import Foundation
import RxSwift

class WeatherAPI {
    
    private let urlStringMain: String = "https://api.openweathermap.org/data/2.5/"
    private let appID: String = "62465b180341e3c7766e720bf327e0aa"
    var typeOfUnit: UnitType
    var typeOfFetch: FetchType
    
    init(unitType: UnitType, fetchType: FetchType) {
        self.typeOfUnit = unitType
        self.typeOfFetch = fetchType
    }
    
    func fetchWeather(cityParameter: String, type: FetchType) -> Observable<[String: Any]> {
        self.typeOfFetch = type
        return fetchWeather(cityParameter: cityParameter)
    }
    
    func fetchWeather(cityParameter: String) -> Observable<[String: Any]> {
        let adjustedCityParameter = cityParameter.replacingOccurrences(of: " ", with: "+", options: .literal, range: nil)
        let params: [String: String] = [
            self.typeOfFetch.parameter(): adjustedCityParameter,
            "units": self.typeOfUnit.rawValue
        ]
        let url = self.createURL(parameters: params)!
        var request: URLRequest = URLRequest(url: url)
        request.httpMethod = "GET"
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // ima i rx ekstenzija
        return Observable.create{ observer in
            let task = session.dataTask(with: request) { data, response, error in
                guard let json: [String: Any] = (try? JSONSerialization.jsonObject(with: data!, options: [])) as? [String : Any] else {
                    observer.onError(error!)
                    return
                }
                observer.onNext(json)
                observer.onCompleted()
            }
            task.resume()
            return Disposables.create()
        }
        
    }
    
    private func createURL(parameters: [String: String]) -> URL? {
        var urlString = self.urlStringMain + self.typeOfFetch.rawValue
        for (name, value) in parameters {
            urlString += name + "=" + value + "&"
        }
        urlString += "APPID=" + self.appID
        guard let url = URL(string: urlString) else {
            print("URL not valid!")
            return nil
        }
        return url
    }
}


enum FetchType: String {
    case Single = "weather?"
    case Group = "group?"
    
    func parameter() -> String {
        switch self {
        case .Single:
            return "q"
        case .Group:
            return "id"
        }
    }
}

enum UnitType: String {
    case Metric = "metric"
    case Imperial = "imperial"
}
