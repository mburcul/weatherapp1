//
//  CityWeatherViewModel.swift
//  WeatherApp
//
//  Created by Marko Burčul on 02/07/2018.
//  Copyright © 2018 Marko Burčul. All rights reserved.
//

import Foundation
import CoreData
import RxSwift
import RxCocoa

class CityWeatherViewModel {
    private let repository: Repo
    var cities: Observable<[City]>
    private let disposeBag: DisposeBag = DisposeBag()
    
    init() {
        self.repository = Repo(typeOfUnit: UnitType.Metric)
        cities = repository.cityFilter.asObservable()
    }
    
    func bindToSearch(publisher: PublishSubject<String>) {
        publisher
            .bind(to: repository.searchObserver)
            .disposed(by: disposeBag)
    }
}

