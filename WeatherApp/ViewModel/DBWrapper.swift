//
//  DBWrapper.swift
//  WeatherApp
//
//  Created by Marko Burčul on 05/07/2018.
//  Copyright © 2018 Marko Burčul. All rights reserved.
//

import Foundation
import CoreData
import AERecord
import RxSwift
import RxCoreData

class DBWrapper {
    
    func reactiveFetchFromDataBase() -> Observable<[City]> {
        let request: NSFetchRequest<City> = City.fetchRequest()
        let descriptor = NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSString.localizedStandardCompare(_: )))
        request.sortDescriptors = [descriptor]
        let cities: Observable<[City]> = AERecord.Context.default.rx.entities(fetchRequest: request).asObservable()
        return cities
    }
    
    func convertAndStoreJson(json: [String: Any]) {
        guard let data = json["list"] else {
            storeInDataBase(data: json)
            return
        }
        if let listOfCities = data as? [[String: Any]] {
            for cityWeatherData in listOfCities {
                storeInDataBase(data: cityWeatherData)
            }
        }
    }
    
    private func storeInDataBase(data: [String: Any]) {
        if  let name = data["name"] as? String,
            let weather = data["weather"] as? [[String: Any]],
            let weatherState = weather[0]["main"] as? String,
            let weathericon = weather[0]["icon"] as? String,
            let main = data["main"] as? [String: Any],
            let pressure = main["pressure"] as? Int,
            let humidity = main["humidity"] as? Int,
            let tempMin = main["temp_min"] as? Int,
            let tempMax = main["temp_max"] as? Int{
            
            let city:City = City.firstOrCreate(with: ["name": name])
            city.humidity = Float(humidity)
            city.pressure = Int32(pressure)
            city.temperatureMax = Int32(tempMax)
            city.temperatureMin = Int32(tempMin)
            city.weatherState = weatherState
            city.iconcode = weathericon
            do {
                try AERecord.Context.default.save()
            } catch {
                print("error")
            }
        }
    }
    
    
}
